from django.apps import AppConfig


class ContohappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'contohapp'
