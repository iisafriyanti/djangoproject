from django.test import TestCase, Client
from django.urls import resolve
from .views import index

class ContohAppTest(TestCase):
	def test_contoh_app_url_is_exist(self):
		response = Client().get('/contohapp')
		self.assertEqual(response.status_code,200)

	def test_contoh_app_using_to_do_list_template(self):
		response = Client().get('/contohapp')
		self.assertTemplateUsed(response, 'index.html')
