from django.shortcuts import render 
from .models import Person, Post


mhs_name = 'Kak Pewe'

def index(request):
    persons = Person.objects.all().values()

    response = {'name' : mhs_name, 'persons' : persons}
    return render(request, 'index.html', response)

def javascript(request):
    response = {'name' : mhs_name}
    return render(request, 'javascript.html', response)