from django.urls import include, path
from .views import index, javascript

urlpatterns = [
    path('', index, name='index'),
    path('/javascript', javascript, name = 'javascript')
]